<?php
include 'db_movie.php';
include 'db_log.php';

if (isset($_POST['submit'])) {
    foreach ($_POST as $key => $value) {
        ${$key} = $value;
    }

    $db_movie = new Db_movie();
    $db_movie->update($id, $judul, $tahun, $genre, $durasi, $overview, $bahasa, $rating, $umur);

    insert_data_log("Mengedit Data \"" . $judul . "\"");

    header("location: detail.php?id=$id");
} else {
    header('location: index.php');
}
