<?php
include 'db_log.php';
include 'db_movie.php';

$db_movie = new Db_movie();

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $data = $db_movie->get_detail($id);
    insert_data_log("Akses Halaman Edit \"" . $data['title'] . "\"");
} else {
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <title>Movie Catalogue - Edit</title>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php"> <span class="nav-btn"> Movie Catalogue </span></a>
            <a class="nav-item nav-link" href="log.php">
                <span class="material-icons nav-btn align-middle">
                    history
                </span>
            </a>
        </div>
    </nav>

    <div class="container my-5">
        <div class="row justify-content-md-center">
            <div class="col-6">
                <h2 class="text-center">Edit Data</h2>
                <form action="prosesedit.php" method="post">
                    <input type="hidden" name="id" value="<?= $data['id'] ?>" readonly>

                    <div class="mb-3">
                        <label for="nama">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" value="<?= $data['title'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="tahun">Tahun Rilis</label>
                        <input type="text" class="form-control" name="tahun" id="tahun" value="<?= $data['date'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="genre">Genre</label>
                        <input type="text" class="form-control" name="genre" id="genre" value="<?= $data['genre'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="durasi">Durasi</label>
                        <input type="text" class="form-control" name="durasi" id="durasi" value="<?= $data['duration'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="overview">Overview</label>
                        <textarea type="text" class="form-control" name="overview" id="overview" rows="3"><?= $data['overview'] ?></textarea>
                    </div>

                    <div class=" mb-3">
                        <label for="bahasa">Bahasa</label>
                        <input type="text" class="form-control" name="bahasa" id="bahasa" value="<?= $data['language'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="rating">Rating</label>
                        <input type="text" class="form-control" name="rating" id="rating" value="<?= $data['rating'] ?>">
                    </div>

                    <div class=" mb-3">
                        <label for="umur">Kategori Umur</label>
                        <input type="text" class="form-control" name="umur" id="umur" value="<?= $data['age'] ?>">
                    </div>

                    <div class=" mb-3">
                        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>