<?php
include 'db_log.php';
include 'db_movie.php';

$db_movie = new Db_movie();

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$data = $db_movie->get_detail($id);
	insert_data_log("Akses Halaman Detail \"" . $data['title'] . "\"");
} else {
	header("location: index.php");
}

?>

<!DOCTYPE html>
<html>

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<title>Movie Catalogue - Detail</title>
</head>

<body>
	<nav class="navbar navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="index.php"> <span class="nav-btn"> Movie Catalogue </span></a>
			<a class="nav-item nav-link" href="log.php">
				<span class="material-icons nav-btn align-middle">
					history
				</span>
			</a>
		</div>
	</nav>
	<div class="container my-5">
		<div class="row justify-content-md-center">
			<div class="col-6">
				<h3><?= $data['title'] ?></h3>
				<div class="shadow-sm p-3 mb-3 bg-body border rounded-3">
					<div class="d-flex justify-content-between">
						<div>
							<p class="border border-dark rounded-3 px-2 py-1"><?= $data['date'] ?></p>
						</div>
						<div>
							<p class="border border-dark rounded-3 px-2 py-1"><?= $data['duration'] ?></p>
						</div>
						<div>
							<p class="border border-dark rounded-3 px-2 py-1"><?= $data['language'] ?></p>
						</div>
						<div>
							<p class="border border-dark rounded-3 px-2 py-1"><?= $data['age'] ?></p>
						</div>
					</div>
					<table>
						<tr>
							<td style="width: 70px;">
								<p>Genre</p>
							</td>
							<td>
								<p><?= $data['genre'] ?></p>
							</td>
						</tr>
						<tr>
							<td style="width: 70px;">
								<p class="align-middle">Rating</p>
							</td>
							<td>
								<p>
									<span class="align-middle"><?= $data['rating'] ?></span>
									<span class="material-icons align-middle" style="color: yellowgreen;">
										grade
									</span>
								</p>
							</td>
						</tr>
					</table>
					<p><?= $data['overview'] ?></p>
				</div>
				<div class="d-flex justify-content-between">
					<div class="align-middle">
						<a href="edit.php?id=<?= $data['id'] ?>" class="btn btn-primary">Edit</a>
					</div>
					<div class="align-middle">
						<form action="proseshapus.php" method="post">
							<input type="hidden" name="id" value="<?= $data['id'] ?>" readonly>
							<button type="submit" name="submit" class="btn btn-danger">Hapus</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>