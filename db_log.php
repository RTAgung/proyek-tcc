<?php
function get_client_ip()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'IP tidak dikenali';
    return $ipaddress;
}

function insert_data_log($message)
{
    $connect = new mysqli('db_log', 'rtagung', '12345678', 'log_db');
    $ip_user = get_client_ip();
    date_default_timezone_set("Asia/Jakarta");
    $activity = $message;
    $date = date('Y-m-d');
    $time = date('H:i:s');
    mysqli_query($connect, "INSERT INTO log VALUES (NULL, '$ip_user', '$activity', '$date', '$time')");
}
