<?php
include 'db_log.php';
insert_data_log("Akses Halaman Log");
?>
<!DOCTYPE html>
<html>

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<title>Movie Catalogue - Log</title>
</head>

<body class="body">
	<nav class="navbar navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="index.php"> <span class="nav-btn"> Movie Catalogue </span></a>
			<a class="nav-item nav-link" href="log.php">
				<span class="material-icons nav-btn align-middle">
					history
				</span>
			</a>
		</div>
	</nav>

	<div class="container my-5">
		<div class="row justify-content-md-center">
			<div class="col-8">
				<h3 class="text-center mb-5">Log</h3>
				<div class="table-responsive">
					<table class="table table-sm">

						<thead>
							<tr>
								<th style="width: 10%;">No.</th>
								<th style="width: 18%;">IP User</th>
								<th style="width: 45%;">Aktivitas</th>
								<th style="width: 15%;">Tanggal</th>
								<th style="width: 12%;">Waktu</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$connect = new mysqli('db_log', 'rtagung', '12345678', 'log_db');
							$link = mysqli_query($connect, "SELECT * FROM log ORDER BY id DESC");
							$i = 1;
							$j = 0;
							while ($tampung = mysqli_fetch_object($link)) {
							?>
								<tr>
									<td scope="row"><?= $i++; ?>.</td>
									<td><?= $tampung->ip_user ?></td>
									<td><?= $tampung->activity ?></td>
									<td><?= $tampung->date ?></td>
									<td><?= $tampung->time ?></td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>



</body>

</html>