<?php
include 'db_log.php';
include 'db_movie.php';

insert_data_log("Akses Halaman Home");

$db_movie = new Db_movie();

$data = $db_movie->get_all();
?>


<!DOCTYPE html>
<html>

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<title>Movie Catalogue</title>
</head>

<body>
	<nav class="navbar navbar-dark bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand" href="index.php"> <span class="nav-btn"> Movie Catalogue </span></a>
			<a class="nav-item nav-link" href="log.php">
				<span class="material-icons nav-btn align-middle">
					history
				</span>
			</a>
		</div>
	</nav>
	<div class="container my-5">
		<div class="d-flex justify-content-between">
			<h2>List Movie</h2>
			<div>
				<a class="btn btn-dark" href="tambah.php">Tambah</a>
			</div>
		</div>

		<div class="row my-4">
			<?php
			foreach ($data as $item) {
			?>
				<div class="col-3">
					<a href="detail.php?id=<?= $item['id'] ?>" class="link">
						<div class="box p-3 mb-4 bg-body border rounded-3">
							<h5><?= $item['title'] ?></h5>
							<div class="d-flex justify-content-between">
								<div>
									<span class="align-middle"><?= $item['date'] ?></span>
								</div>
								<div>
									<span class="align-middle"><?= $item['rating'] ?></span>
									<span class="material-icons align-middle" style="color: yellowgreen;">
										grade
									</span>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php
			}
			?>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>
