<?php
include "db_movie.php";
include 'db_log.php';

if (isset($_POST['submit'])) {
    foreach ($_POST as $key => $value) {
        ${$key} = $value;
    }

    $db_movie = new Db_movie();
    $db_movie->insert($judul, $tahun, $genre, $durasi, $overview, $bahasa, $rating, $umur);

    insert_data_log("Menambahkan Data \"" . $judul . "\"");
}
header('location: index.php');

?>