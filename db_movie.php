<?php

class Db_movie
{
    function __construct()
    {
        $hostname = "db_movie";
        $username = "rtagung";
        $password = "12345678";
        $database = "movie_db";
        $this->connect = mysqli_connect($hostname, $username, $password, $database);
    }

    function get_all()
    {
        $query = "SELECT * FROM movie";
        $data = mysqli_query($this->connect, $query);
        $hasil = array();
        while ($d = mysqli_fetch_array($data)) {
            $hasil[] = $d;
        }
        return $hasil;
    }

    function get_detail($id)
    {
        $query = "SELECT * FROM movie WHERE id = '$id'";
        $data = mysqli_query($this->connect, $query);
        $hasil = null;
        while ($d = mysqli_fetch_array($data)) {
            $hasil = $d;
        }
        return $hasil;
    }

    function insert($title, $date, $genre, $duration, $overview, $language, $rating, $age)
    {
        $query = "INSERT INTO movie VALUES (NULL, '$title', '$date', '$genre', '$duration', " . "$overview" . ", '$language', '$rating', '$age');";
        mysqli_query($this->connect, $query);
    }

    function update($id, $title, $date, $genre, $duration, $overview, $language, $rating, $age)
    {
        $query = "UPDATE movie SET title = '$title', date = '$date', genre = '$genre', duration = '$duration', overview = '$overview', language = '$language', rating = '$rating', age = '$age' WHERE id = '$id'";
        mysqli_query($this->connect, $query);
    }

    function delete($id)
    {
        $query = "DELETE FROM movie WHERE id = '$id'";
        mysqli_query($this->connect, $query);
    }
}
